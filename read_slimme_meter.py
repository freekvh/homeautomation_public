#!/usr/bin/python3
"""
A script to read the slimme meter and send out the results via mqtt.
"""

import serial
import sys
import paho.mqtt.client as mqtt
import time

# Dictionary of the data we need
code2value = {'1-0:1.8.1'  : 'laag_tarief_meter_geleverd',
              '1-0:1.8.2'  : 'hoog_tarief_meter_geleverd',
              '1-0:1.7.0'  : 'huidig_verbruik',
              '0-1:24.2.1' : 'gasmeterstand'}

# Configure the parameters needed to read the port
ser = serial.Serial()
ser.baudrate = 115200
ser.port="/dev/ttyUSB0"

# Open the connection
ser.open()

# Initiate mqtt connection
broker_address="home.hmrt.nl" # Set the mqtt broker address
client = mqtt.Client("slimmemeter") #create new instance
#client.username_pw_set('', password='')
client.connect(broker_address) #connect to broker
# Capture one full telegram, terminate at the first "!"
telegram = []
line = '' 
while '!' not in line:
    line = ser.readline().decode('utf-8')
    telegram.append(line)

# Select only the values of interest, add to a new dictionary, we do this because
# we first want to combime hoog and laag tarief before publishing
quantity2value = dict()
for line in telegram:
    for code in code2value:
        if code in line:
            value    = float(line.split('(')[-1].split('*')[0])
            unit     = line.split('(')[-1].split('*')[-1].replace(')','')
            quantity = code2value[code]
            quantity2value[quantity] = value
# Compute the total "energie geleverd"
quantity2value['energie_geleverd'] = quantity2value['laag_tarief_meter_geleverd'] + quantity2value['hoog_tarief_meter_geleverd']

# Define the quantities of interest and publish using mqtt
quantities = ['huidig_verbruik', 'energie_geleverd', 'gasmeterstand']
for quantity in quantities:
    #print(quantity, round(quantity2value[quantity], 3))
    client.publish('home/smart_meter/'+quantity, round(quantity2value[quantity], 3)) #publish
    print(quantity, round(quantity2value[quantity], 3))
    time.sleep(2)
client.disconnect()
